//  ALAnimationDirection.swift
//  ALAnimationController
//
//  Created by Alex Hmelevski on 2017-01-23.
//  Copyright © 2017 Alex Hmelevski. All rights reserved.
//

import Foundation

struct ALAnimationDirection: OptionSet {
    let rawValue: Int
    static let up = ALAnimationDirection(rawValue: 1 << 0)
    static let down = ALAnimationDirection(rawValue: 1 << 1)
    static let left = ALAnimationDirection(rawValue: 1 << 2)
    static let rigth = ALAnimationDirection(rawValue: 1 << 3)

}

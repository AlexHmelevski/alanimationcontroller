//
//  ALInteractiveAutocompletor.swift
//  ALAnimationController
//
//  Created by Alex Hmelevski on 2017-01-25.
//  Copyright © 2017 Alex Hmelevski. All rights reserved.
//

import Foundation
import UIKit

protocol IInteractiveAnimatorAutoCompletorDelegate: class {
    func interactionStarted()
}

protocol IInteractiveAnimatorAutoCompletor {
    func associate(animatedView: UIView,
                   finalFrame: CGRect,
                   inContainer container: UIView,
                   direction: ALAnimationDirection,
                   completion: ((Bool, AnimatorState) -> Void)?)
    func addTransformers(transformers: [IViewAnimatedTransformer])
    weak var delegate: IInteractiveAnimatorAutoCompletorDelegate? { get set }
}

class ALInteractiveAutocompletor: NSObject, IInteractiveAnimatorAutoCompletor, UICollisionBehaviorDelegate {
    private var startLocation: CGPoint = .zero
    private var previousPoint: CGPoint = .zero
    private var panGesture: UIPanGestureRecognizer!
    private var animatedView: UIView!
    private var container: UIView!
    private var direction: ALAnimationDirection  = []
    private var completion: ((Bool, AnimatorState) -> Void)?
    private var finalFrame: CGRect!
    private var viewTransformers: [IViewAnimatedTransformer] = []
    var delegate: IInteractiveAnimatorAutoCompletorDelegate?

    func associate(animatedView: UIView,
                   finalFrame: CGRect,
                   inContainer container: UIView,
                   direction: ALAnimationDirection,
                   completion: ((Bool, AnimatorState) -> Void)?) {
        self.finalFrame = finalFrame
        self.animatedView = animatedView
        self.container = container
        self.completion = completion
        self.panGesture = UIPanGestureRecognizer(target: self, action: #selector(ALInteractiveAutocompletor.handleSwipeUpdate))
        self.panGesture.maximumNumberOfTouches = 1
        self.direction = direction
        startLocation = animatedView.frame.origin
        animatedView.addGestureRecognizer(panGesture)
    }

    func addTransformers(transformers: [IViewAnimatedTransformer]) {
        viewTransformers.append(contentsOf: transformers)
    }

    @objc
    func handleSwipeUpdate(gestureRecognizer: UIPanGestureRecognizer) {

        var finalFrame: CGRect = .zero
        let loc = gestureRecognizer.location(in: container)
        let dY = loc.y - previousPoint.y

        let nFrame = animatedView.frame.offsetBy(dx: 0, dy: dY)
        switch gestureRecognizer.state {
        case .began : previousPoint = loc
            delegate?.interactionStarted()
        case .changed:

            if container.frame.origin.y <= nFrame.origin.y {
                animatedView.frame = nFrame
                let progress = (container.frame.size.height - nFrame.origin.y) / container.frame.size.height
                self.viewTransformers.forEach({ $0.updateProgress(progress: progress) })
            }

        default:
            // @TODO: Make it more abstract, to be able to work in different directions

            if container.frame.height * 0.7 <= (container.frame.height - nFrame.origin.y) {
                finalFrame = container.frame
            } else if (startLocation.y + startLocation.y * 0.2) < nFrame.origin.y {
                finalFrame = container.frame
                finalFrame.origin = CGPoint(x: finalFrame.origin.x, y: finalFrame.size.height)
            } else if (startLocation.y - startLocation.y * 0.2) >= nFrame.origin.y {
                finalFrame = container.frame
                finalFrame.origin = CGPoint(x: finalFrame.origin.x, y: startLocation.y)
            } else {
                finalFrame = container.frame
                finalFrame.origin = CGPoint(x: finalFrame.origin.x, y: startLocation.y)
            }
            let progress = (container.frame.size.height - finalFrame.origin.y) / container.frame.size.height

            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: [], animations: {
                self.animatedView.frame = finalFrame
                self.viewTransformers.forEach({ $0.updateProgress(progress: progress) })
            }, completion: { (done) in
                if self.animatedView.frame == self.container.frame {
                    self.animatedView.removeGestureRecognizer(self.panGesture)
                    self.completion?(done, .presenting)

                }
                if !self.animatedView.frame.intersects(self.container.frame) {
                    self.completion?(done, .dismissing)
                }
            })

        }
        previousPoint = loc
    }
}

//
//  ALViewsProvider.swift
//  ALAnimationController
//
//  Created by Alex Hmelevski on 2017-01-24.
//  Copyright © 2017 Alex Hmelevski. All rights reserved.
//

import Foundation
import UIKit

protocol IViewsProvider {
    func views(fromContext context: UIViewControllerContextTransitioning,
                    forState state: AnimatorState) -> (toView: UIView, fromView: UIView)
    func viewControllers(fromContext context: UIViewControllerContextTransitioning,
                              forState state: AnimatorState) -> (to: UIViewController, from: UIViewController)
}

final class ALViewsProvider: IViewsProvider {

    func views(fromContext context: UIViewControllerContextTransitioning,
                    forState state: AnimatorState) -> (toView: UIView, fromView: UIView) {
        let vcs = self.vcs(fromContext: context, forState: state)
        return (vcs.to.view, vcs.from.view)
    }

    func viewControllers(fromContext context: UIViewControllerContextTransitioning,
                              forState state: AnimatorState) -> (to: UIViewController, from: UIViewController) {
        return vcs(fromContext: context, forState: state)
    }

    private func vcs(fromContext context: UIViewControllerContextTransitioning,
                          forState state: AnimatorState) -> (to: UIViewController, from: UIViewController) {
        guard let fromVC = context.viewController(forKey: UITransitionContextViewControllerKey.from),
            let toVC = context.viewController(forKey: .to) else { fatalError("NOT ENOUGHT CONTROLLERS in \(#function)") }
        switch state {
        case .presenting: return (toVC, fromVC)
        case .dismissing: return (fromVC, toVC)
        }
    }
}

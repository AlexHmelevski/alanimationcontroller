//
//  IViewTransformer.swift
//  ALAnimationController
//
//  Created by Alex Hmelevski on 2017-01-23.
//  Copyright © 2017 Alex Hmelevski. All rights reserved.
//

import Foundation
import UIKit

protocol IViewTransformer {
    var widthMultiplier: Double { get }
    var heightMultiplier: Double { get }
    func transformViewDuringPresenting(view: UIView)
    func transformViewDuringDismissing(view: UIView)
    init()
}

protocol IViewAnimatedTransformer {
    init(view: UIView)
    func updateProgress(progress: CGFloat)
}

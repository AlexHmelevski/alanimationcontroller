//
//  ALSlideViewTransformer.swift
//  ALAnimationController
//
//  Created by Alex Hmelevski on 2017-01-23.
//  Copyright © 2017 Alex Hmelevski. All rights reserved.
//

import Foundation

import UIKit

final class ALSlideContentViewTransformer: IViewTransformer {
    var widthMultiplier: Double {
        return 1
    }

    var heightMultiplier: Double {
        return 1
    }

    func transformViewDuringDismissing(view: UIView) {

    }

    func transformViewDuringPresenting(view: UIView) {

    }

}

final class ALDimminigViewOpacityTransformer: IViewAnimatedTransformer {

    private let animation = CABasicAnimation(keyPath: "opacity")
    private var startOpacity: CGFloat = 0.0
    private let endOpacity: CGFloat = 1.0
    private weak var view: UIView?
    init(view: UIView) {
        self.view = view
        startOpacity = view.alpha

    }

    func updateProgress(progress: CGFloat) {
        view?.alpha = progress
    }
}

//
//  ALAnimationFrameProvider.swift
//  ALAnimationController
//
//  Created by Alex Hmelevski on 2017-01-23.
//  Copyright © 2017 Alex Hmelevski. All rights reserved.
//

import Foundation
import UIKit

protocol IAnimationFrameProvider {
    init()
    var heightPercentage: CGFloat { get set }
    func initialFrame(forInitialFrame frame: CGRect, inFrame: CGRect, direction: ALAnimationDirection, state: AnimatorState) -> CGRect
    func finalFrame(forInitialFrame frame: CGRect, inFrame: CGRect, direction: ALAnimationDirection, state: AnimatorState) -> CGRect
}

final class ALAnimationFrameProvider: IAnimationFrameProvider {
    var heightPercentage: CGFloat = 1
    func initialFrame(forInitialFrame frame: CGRect, inFrame: CGRect, direction: ALAnimationDirection, state: AnimatorState) -> CGRect {
        var newFrame = frame
        let presenting = (state == .presenting)

        if direction.contains(.up) {
            newFrame.origin.y = presenting ? inFrame.size.height : frame.origin.y
        }

        if direction.contains(.down) {
            newFrame.origin.y = presenting ? -frame.size.height : frame.origin.y
        }

        if direction.contains(.left) {
            newFrame.origin.x = presenting ? inFrame.size.width : frame.size.width
        }

        if direction.contains(.rigth) {
            newFrame.origin.x = presenting ? -frame.size.width : frame.origin.x
        }

        return newFrame
    }

    func finalFrame(forInitialFrame frame: CGRect, inFrame: CGRect, direction: ALAnimationDirection, state: AnimatorState) -> CGRect {
        var newFrame = frame
        let presenting = (state == .presenting)
        if direction.contains(.up) {
            newFrame.origin.y = presenting ? inFrame.size.height - inFrame.size.height * heightPercentage : inFrame.size.height
        }

        if direction.contains(.down) {
            newFrame.origin.y = presenting ? 0 : -frame.size.height
        }

        if direction.contains(.left) {
            newFrame.origin.x = presenting ? frame.origin.x : inFrame.size.width
        }

        if direction.contains(.rigth) {
            newFrame.origin.x = presenting ? 0 : -frame.size.width
        }

        return newFrame
    }

}

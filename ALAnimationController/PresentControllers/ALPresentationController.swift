//
//  ALPresentationController.swift
//  ALAnimationController
//
//  Created by Alex Hmelevski on 2017-01-23.
//  Copyright © 2017 Alex Hmelevski. All rights reserved.
//

import UIKit
protocol UIPresentationControllerDelegate: class {
    func presentationTransitionDidEnd(_ completed: Bool)
    func dismissalTransitionDidEnd(_ completed: Bool)
    func interactionStarted()
}

final class ALPresentationControllerBuilder {
    var contentViewTransformer: IViewTransformer = ALSlideContentViewTransformer()
    var dimmingViewTransformerTypes: [IViewAnimatedTransformer.Type] = [ALDimminigViewOpacityTransformer.self]
    var dimmingView: UIView.Type = UIView.self
    var presentedViewController: UIViewController!
    var presentingViewController: UIViewController!
    var directions: ALAnimationDirection = []
    var interactiveController: ALInteractiveTransitioner!
    weak var controllerDelegate: UIPresentationControllerDelegate?
    var percentage: CGFloat = 1.0

    var controller: ALPresentationController {
        let control = ALPresentationController(presentedViewController: presentedViewController, presenting: presentingViewController)
        control.contentViewTransformer = contentViewTransformer
        control.dimmingTransformerTypes = dimmingViewTransformerTypes
        control.directions = directions
        control.presentationDelegate = controllerDelegate
        control.percentage = percentage
        control.interactiveController = interactiveController
        return control
    }
}

class ALPresentationController: UIPresentationController, IInteractiveAnimatorAutoCompletorDelegate {
    var autocompletor: IInteractiveAnimatorAutoCompletor = ALInteractiveAutocompletor()
    var interactiveController: ALInteractiveTransitioner!

    fileprivate var dimmingView: UIView = UIView()
    fileprivate var directions: ALAnimationDirection!
    fileprivate var contentViewTransformer: IViewTransformer!

    fileprivate weak var presentationDelegate: UIPresentationControllerDelegate?
    fileprivate var percentage: CGFloat!
    fileprivate var dimmingTransformerTypes: [IViewAnimatedTransformer.Type] = [ALDimminigViewOpacityTransformer.self]
    private var panGesture: UIPanGestureRecognizer!

    private(set) var interactive: Bool = false
    private var dimmingTransformers: [IViewAnimatedTransformer] = []

    override init(presentedViewController: UIViewController,
                  presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        self.prepareDimmingView()
        autocompletor.delegate = self

    }

    override func presentationTransitionWillBegin() {
        super.presentationTransitionWillBegin()
        guard let containerView = self.containerView else { return }
        self.dimmingView.frame = containerView.bounds
        self.dimmingView.alpha = 0.0
        containerView.insertSubview(self.dimmingView, at: 0)

        self.presentedViewController.transitionCoordinator?.animate(alongsideTransition: { (_) -> Void in
            self.dimmingTransformers.forEach({ $0.updateProgress(progress: self.percentage) })
        }, completion: nil)
    }

    override func presentationTransitionDidEnd(_ completed: Bool) {
        super.presentationTransitionDidEnd(completed)
        if percentage < 1 {
            guard let view = presentedView,
                   let container = containerView else { return }
            autocompletor.associate(animatedView: view,
                                    finalFrame: container.frame,
                                    inContainer: container,
                                    direction: directions,
                                    completion: self.interactionFinished)
        } else {
            installGestures()
        }
    }

    override func dismissalTransitionWillBegin() {
        super.dismissalTransitionWillBegin()
        self.presentedViewController.transitionCoordinator?.animate(alongsideTransition: { (_) -> Void in
            self.dimmingTransformers.forEach({ $0.updateProgress(progress: 0) })
        }, completion: nil)
    }

    override func dismissalTransitionDidEnd(_ completed: Bool) {
        super.dismissalTransitionDidEnd(completed)
        self.presentationDelegate?.dismissalTransitionDidEnd(completed)
    }

    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        guard let wMuplty = self.contentViewTransformer?.widthMultiplier,
            let hMulty = self.contentViewTransformer?.heightMultiplier else {
                return super.size(forChildContentContainer: container, withParentContainerSize: parentSize)
        }

        return CGSize(width: parentSize.width * CGFloat(wMuplty), height: parentSize.height * CGFloat(hMulty))
    }

    override var frameOfPresentedViewInContainerView: CGRect {
        var presentedViewFrame = CGRect.zero
        guard let containerBounds = self.containerView?.bounds else { return presentedViewFrame }
        presentedViewFrame.size = self.size(forChildContentContainer: self.presentedViewController, withParentContainerSize: containerBounds.size)

        if directions.contains(.up) {
            presentedViewFrame.origin.y = (containerBounds.size.height - presentedViewFrame.size.height)
        }

        if directions.contains(.down) {
            presentedViewFrame.origin.y = 0
        }

        if directions.contains(.left) {
            presentedViewFrame.origin.x = containerBounds.size.width - presentedViewFrame.size.width
        }

        if directions.contains(.rigth) {
            presentedViewFrame.origin.x = 0
        }

        return presentedViewFrame
    }

    private func prepareDimmingView() {
        // @TODO: remove this initial code for color set-up in dimming view, should be passed from the outside

        self.dimmingView.backgroundColor = UIColor(white: 0.0, alpha: 0.4)
        self.dimmingView.alpha = 0.0
        let gesture = UITapGestureRecognizer(target: self, action: #selector(ALPresentationController.dimmingViewTapped))
        self.dimmingView.addGestureRecognizer(gesture)
        dimmingTransformers = dimmingTransformerTypes.map({ $0.init(view: dimmingView) })
        autocompletor.addTransformers(transformers: dimmingTransformers)
    }

    func dimmingViewTapped() {
        self.presentedViewController.dismiss(animated: true, completion: nil)
    }

    func interactionFinished(completed: Bool, state: AnimatorState) {
        switch state {
        case .dismissing: self.presentedViewController.dismiss(animated: true, completion: nil)
        case .presenting:
            interactive = true
            installGestures()
            self.presentationDelegate?.presentationTransitionDidEnd(completed)
        }
    }

    func installGestures() {
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(ALInteractiveTransitioner.handleSwipeUpdate))
        panGesture.maximumNumberOfTouches = 1
        presentedViewController.view.addGestureRecognizer(self.panGesture)
    }

    func handleSwipeUpdate(gestureRecognizer: UIPanGestureRecognizer) {

        switch gestureRecognizer.state {
        case .began:
            presentedViewController.dismiss(animated: true, completion: nil)
        default: interactiveController.handleSwipeUpdate(gestureRecognizer: gestureRecognizer)

        }
    }

    func interactionStarted() {
        presentationDelegate?.interactionStarted()
    }
}

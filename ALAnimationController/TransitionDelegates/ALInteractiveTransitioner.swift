//
//  ALInteractiveTransitioner.swift
//  ALAnimationController
//
//  Created by Alex Hmelevski on 2017-01-24.
//  Copyright © 2017 Alex Hmelevski. All rights reserved.
//

import Foundation
import QuartzCore
import UIKit

final class ALInteractiveTransitioner: UIPercentDrivenInteractiveTransition, UIGestureRecognizerDelegate {
    private var contextData: UIViewControllerContextTransitioning!
    private let viewsProvider: IViewsProvider
    private var startLocation: CGPoint = .zero
    private let state: AnimatorState
    private let directions: ALAnimationDirection

    init(state: AnimatorState, directions: ALAnimationDirection, viewsProvider: IViewsProvider = ALViewsProvider()) {
        self.state = state
        self.directions = directions
        self.viewsProvider = viewsProvider
        super.init()
    }

    override func startInteractiveTransition(_ transitionContext: UIViewControllerContextTransitioning) {
        contextData = transitionContext
        super.startInteractiveTransition(transitionContext)
    }

    func handleSwipeUpdate(gestureRecognizer: UIPanGestureRecognizer) {

        switch gestureRecognizer.state {
        case .began: break
        case .changed:
            guard let contextData = contextData else { return }
            let container = contextData.containerView
            let per = progress(forLocation: gestureRecognizer.location(in: container))
            update(per)
        default:
            print(percentComplete)
            if percentComplete > 0.3 {
                finish()
            }

            if percentComplete < 0.3 {
                cancel()
            }
        }
    }

    func progress(forLocation: CGPoint) -> CGFloat {
        let vcs = viewsProvider.viewControllers(fromContext: contextData, forState: state)
        let frame = contextData.finalFrame(for: vcs.to)

//        var nextFrame = frame
//        let dX = forLocation.x - startLocation.x
        let dY = forLocation.y - startLocation.y

        let finalFrame = contextData.finalFrame(for: vcs.to)
        var progress = percentComplete

        if directions.contains(.up) {
           progress = (finalFrame.size.height - (frame.origin.y + dY)) / finalFrame.size.height
        }

        if directions.contains(.down) {
            progress = (frame.origin.y + dY) / finalFrame.size.height
        }

        if directions.contains(.left) {
            fatalError("NOT IMPLEMENTED")
        }

        if directions.contains(.rigth) {
            fatalError("NOT IMPLEMENTED")
        }

        return progress
    }

}

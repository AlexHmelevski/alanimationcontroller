//
//  ALSlideAnimationTransitioning.swift
//  ALAnimationController
//
//  Created by Alex Hmelevski on 2017-01-23.
//  Copyright © 2017 Alex Hmelevski. All rights reserved.
//

import Foundation
import QuartzCore
import UIKit

class ALSlideAnimationDelegate: NSObject, UIViewControllerAnimatedTransitioning {
    var state: AnimatorState
    let _duration: Double
    let directions: ALAnimationDirection
    var frameProvider: IAnimationFrameProvider
    var context: UIViewControllerContextTransitioning!
    var viewsProvider: IViewsProvider = ALViewsProvider()
    var percentage: CGFloat
    var animatedView: UIView!
    var container: UIView!

    init(state: AnimatorState,
         directions: ALAnimationDirection = [.up],
           duration: Double = 0.9,
        frameProviderType: IAnimationFrameProvider.Type = ALAnimationFrameProvider.self,
        percentage: CGFloat = 1) {
        self.state = state
        self._duration = duration
        self.directions = directions
        self.frameProvider = frameProviderType.init()
        self.percentage = percentage
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return self._duration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        // Here, we perform the animations necessary for the transition
        guard let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
            let fromView = fromVC.view,
            let toVC = transitionContext.viewController(forKey: .to),
            let toView = toVC.view else { return }

        if case .presenting = state {
            transitionContext.containerView.addSubview(toView)
        }

        let vcs = viewsProvider.viewControllers(fromContext: transitionContext, forState: state)

        guard let animatingView = vcs.to.view else { return }
        self.animatedView = animatingView
        self.container = transitionContext.containerView
        let appearedFrame = transitionContext.finalFrame(for: vcs.to)
        frameProvider.heightPercentage = percentage
        let initialFrame = frameProvider.initialFrame(forInitialFrame: appearedFrame,
                                                      inFrame: vcs.to.view.frame,
                                                      direction: directions,
                                                      state: state)

        let finalFrame = frameProvider.finalFrame(forInitialFrame: appearedFrame,
                                                  inFrame: vcs.to.view.frame,
                                                  direction: directions,
                                                  state: state)
        animatingView.frame = initialFrame

        UIView.animate(withDuration: self.transitionDuration(using: transitionContext),
                       delay: 0,
                       options: [.allowUserInteraction],
                       animations: { () -> Void in animatingView.frame = finalFrame }) { (_) in

                        if self.state == .dismissing && !transitionContext.transitionWasCancelled {
                            fromView.removeFromSuperview()
                        }

                        transitionContext.completeTransition(!transitionContext.transitionWasCancelled)

        }
    }
}

//
//  ALSlideTransitionDelegate.swift
//  ALAnimationController
//
//  Created by Alex Hmelevski on 2017-01-23.
//  Copyright © 2017 Alex Hmelevski. All rights reserved.
//

import Foundation
import UIKit

enum AnimatorState {
    case presenting
    case dismissing
}

protocol ALSlideTransitionManagerDelegate: class {
    func interactiveStarted()
}

class ALSlideTransitionManager: NSObject, UIViewControllerTransitioningDelegate, UIPresentationControllerDelegate {
    private let directions: ALAnimationDirection
    private let controllerBuilder: ALPresentationControllerBuilder
    private var percentage: CGFloat
    private var controller: ALPresentationController!
    private let initialPercentage: CGFloat
    weak var delegate: ALSlideTransitionManagerDelegate?

    init(directions: ALAnimationDirection = [.up],
         controllerProvider: ALPresentationControllerBuilder = ALPresentationControllerBuilder(),
         partialPercentage: CGFloat = 0.5) {
        self.directions = directions
        controllerBuilder = controllerProvider
        controllerBuilder.directions = directions
        percentage = partialPercentage
        initialPercentage = partialPercentage
    }

    func presentationController(forPresented presented: UIViewController,
                                            presenting: UIViewController?,
                                                source: UIViewController) -> UIPresentationController? {
        controllerBuilder.presentedViewController = presented
        controllerBuilder.presentingViewController = presenting
        controllerBuilder.percentage = percentage
        controllerBuilder.controllerDelegate = self
        controller = controllerBuilder.controller
        return controller
    }

    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        if controller.interactive {
            controller.interactiveController = ALInteractiveTransitioner(state: .dismissing, directions: [.down])
        }
        return controller.interactive  ? controller.interactiveController : nil
    }

    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return nil
    }

    func animationController(forPresented presented: UIViewController,
                                         presenting: UIViewController,
                                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animator = ALSlideAnimationDelegate(state: .presenting, directions: directions)
        animator.state = .presenting
        animator.percentage = percentage

        return animator
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return ALSlideAnimationDelegate(state: .dismissing, directions: directions)
    }

    func presentationTransitionDidEnd(_ completed: Bool) {

        if completed {
           percentage = 1.0
        }

    }

    func dismissalTransitionDidEnd(_ completed: Bool) {
        if completed {
          percentage = initialPercentage
        }

    }
    func interactionStarted() {
        delegate?.interactiveStarted()
    }
}

//  ViewController.swift
//  ALAnimationController
//
//  Created by Alex Hmelevski on 2017-01-23.
//  Copyright © 2017 Alex Hmelevski. All rights reserved.
//

import UIKit

class ViewController: UIViewController, ALSlideTransitionManagerDelegate {
    private var trDeleg: ALSlideTransitionManager = ALSlideTransitionManager()
    private var timer: Timer!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func show(_ sender: Any) {
//        trDeleg = ALSlideTransitionDelegate()
        let newVC = ViewController2()
        newVC.transitioningDelegate = trDeleg
        trDeleg.delegate = self
        present(newVC, animated: true, completion: nil)
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: false, block: { (_) in
            newVC.dismiss(animated: true, completion: nil)
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }

    func interactiveStarted() {
        timer.invalidate()
    }

}

class ViewController2: UIViewController {

    init() {
        super.init(nibName: nil, bundle: nil)
        let tv = TextField(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        tv.backgroundColor = .red

        let but = UIButton(frame: CGRect(x: 0, y: 100, width: 100, height: 50))
        but.backgroundColor = .yellow
        but.setTitle("TEST", for: .normal)
        but.addTarget(self, action: #selector(ViewController2.test), for: .touchUpInside)
        view.addSubview(tv)
        view.addSubview(but)
        modalPresentationStyle = .custom
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .blue
        view.isUserInteractionEnabled = true
        // Do any additional setup after loading the view, typically from a nib.
    }
    func test() {
        print("WORKED")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

}
